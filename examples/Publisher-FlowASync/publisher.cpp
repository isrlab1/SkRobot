#include "publisher.h"

ConstructorImpl(Publisher, SkFlowAsync)
{
    userName = "User1";
    userPasswd = "password";
    outputChanName = "Talker";
    canonicalOutputChanName = "User1.Talker";
    outputChan = -1;
    outputEnabled = false;
    count = 0;

    setObjectName("Publisher");

    SlotSet(init);
    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);
    SlotSet(exitFromDisconnection);
    SlotSet(exitFromKernelSignal);

    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);
    Attach(skApp->kernel_SIG, pulse, this, exitFromKernelSignal, SkQueued);
}

SlotImpl(Publisher, init)
{
    SilentSlotArgsWarning();

    if (!tcpConnect("127.0.0.1", 9000)
        || !getLoginSeed()
        || !login(userName.c_str(), userPasswd.c_str()))
    {
        skApp->quit();
        return;
    }

    //outputChan will be filled only when the client add the channel
    addStreamingChannel(outputChan, FT_BLOB, T_STRING, outputChanName.c_str(), "text/plain");
    //outputChan is still -1 here!

    Attach(this, disconnected, this, exitFromDisconnection, SkQueued);
}

void Publisher::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);

    if (ch->name == canonicalOutputChanName)
        //outputChan is filled by backend (now is > -1)
        ObjectMessage("Output channel is READY");
}

void Publisher::onChannelPublishStartRequest(SkFlowChanID chanID)
{
    if (chanID == outputChan)
    {
        outputEnabled = true;
        count = 0;
        ObjectMessage("Request to START publish on output channel");
    }
}

void Publisher::onChannelPublishStopRequest(SkFlowChanID chanID)
{
    if (chanID == outputChan)
    {
        outputEnabled = false;
        ObjectMessage("Request to STOP publish on output channel");
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Publisher, fastTick)
{
    SilentSlotArgsWarning();

    if (outputEnabled)
    {
        SkString msg = "Hello World: ";
        msg.concat(count);
        publish(outputChan, msg);
        ObjectMessage("Publishing \"" << msg << "\"");
        count++;
    }

}

SlotImpl(Publisher, slowTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(Publisher, oneSecTick)
{
    SilentSlotArgsWarning();
    checkService();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(Publisher, exitFromDisconnection)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting because source is disconnected ..");
    skApp->quit();
}

SlotImpl(Publisher, exitFromKernelSignal)
{
    SilentSlotArgsWarning();

    if (skApp->kernel_SIG->pulse_SIGNAL.getParameters()[0].toInt32() != SIGINT)
        return;

    ObjectMessage("Forcing application exit with CTRL+C");
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

