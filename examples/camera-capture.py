#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

###############################################################################

import cv2      as cv

###############################################################################
# GLOBAL

userName = "User1"
userPasswd = "password"

sat = FlowSat()

longChanName = "User1.Camera.MJPEG"

cap = None
camID = 0
videoOutChan = None
compressionParams = None

w = -1
h = -1
fps = -1
tickInterval = -1

###############################################################################
# SKETCH

def setup():
    global cap
    global w
    global h
    global fps
    global tickInterval
    global compressionParams

    print("[SETUP] ..")
    
    sat.setLogin(userName, userPasswd)
    sat.setNewChanCallBack(onChannelAdded)
    ok = sat.connect()

    if ok:
        sat.setSpeedMonitorEnabled(True)
        cap = cv.VideoCapture(0)

        if not cap.isOpened():
            print("CANNOT open the camera [ID: {}]".format(camID))
            return False

        # basing on camera device, choice the right parameters; these values could not be optimal for yur camera

        #cap.set(cv.CAP_PROP_FOURCC, cv.VideoWriter_fourcc(*'MJPG'))
        cap.set(cv.CAP_PROP_FRAME_WIDTH, 640)
        cap.set(cv.CAP_PROP_FRAME_HEIGHT, 360)
        
        w = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
        h = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
        fps = cap.get(cv.CAP_PROP_FPS)

        compressionParams = [cv.IMWRITE_JPEG_QUALITY, 40]

        tickInterval = 1/fps

        sat.setTickTimer(0, 0) # let wait from camera-device
        #sat.setTickTimer(tickInterval, tickInterval*20)

        res = "{}x{}".format(w, h)

        props = {
            "resolution" : res,
            "fps" : fps,
            "frameRawSize" : int(w * h * 3),
            "tickTimePeriod" : tickInterval,
            "stream" : "MULTIPART",
            "codec" : "JPEG"
        }

        print("Camera setup [Res: {}, FPS: {}]".format(res, fps))
        sat.addStreamingChannel(Flow_T.FT_VIDEO_DATA, Variant_T.T_BYTEARRAY, "Camera.MJPEG", "image/jpeg", props)

        print("[LOOP] ..")
    
    return ok

def loop():
    if videoOutChan is None or not videoOutChan.isPublishingEnabled:
        sat.tick()
        return sat.isConnected()

    ret, f = cap.read()

    if not ret:
        print("CANNOT capture current frame")
        return False

    ok, jpg = cv.imencode('.jpg', f, compressionParams)

    if ok:
        sat.publish(videoOutChan.chanID, bytearray(jpg))
        
    else:
        print("Image encoding error")


    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global videoOutChan

    if ch.name == longChanName:
        videoOutChan = ch
        print("Channel {} CREATED [ChanID: {}]".format(videoOutChan.name, videoOutChan.chanID))
    
###############################################################################




