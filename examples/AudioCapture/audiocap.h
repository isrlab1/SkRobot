#ifndef AUDIOCAP_H
#define AUDIOCAP_H

/******************************************************************************
**
** Copyright (C) Daniele Di Ottavio (aka Tetsuo)
** Contact: tetsuo.tek (at) gmail (dot) com
**
** This program is free software; you can redistribute it and/or
** modify it under the terms of the GNU General Public License
** as published by the Free Software Foundation; either version 3
** of the License, or (at your option) any later version.
**
** This program is distributed in the hope that it will be useful,
** but WITHOUT ANY WARRANTY; without even the implied warranty of
** MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
** GNU General Public License for more details.
**
** You should have received a copy of the GNU General Public License
** along with this program; if not, write to the Free Software
** Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
**
******************************************************************************/

#include <Core/App/skapp.h>
#include <Multimedia/Audio/PA/skportaudio.h>
#include <Core/System/Network/FlowNetwork/skflowsat.h>

class AudioCap extends SkFlowSat
{
    SkString device;
    SkAudioParameters params;
    SkVariant_T data_t;
    SkRingBuffer *input;
    SkPortAudio *pa;
    SkString outputChanName;
    SkFlowChanID outputChanID;
    SkFlowChannel *outputChan;
    SkArgsMap outputChanProps;

    public:
        Constructor(AudioCap, SkFlowSat);

    private:
        bool onSetup()                              override;
        void onInit()                               override;
        void onQuit()                               override;
        void onChannelAdded(SkFlowChanID chanID)    override;
        void onFastTick()                           override;

        bool rec();
        void stop();
};

#endif // AUDIOCAP_H
