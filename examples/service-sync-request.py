#!/usr/bin/env pysketch-executor

####################################################
#SKETCH
from PySketch.abstractflow import FlowChannel
from PySketch.flowproto import FlowChanID
from PySketch.flowsat import FlowSat

import argparse

userName = "guest"
userPasswd = "password"

sat = FlowSat() 
serviceName = None

args = None

parser = argparse.ArgumentParser(description="Send a service channel request and obtain an answer")
parser.add_argument('sketchfile', help='Sketch program file')
parser.add_argument('--service-chan', help='Flow-network service', default='User1.ServiceTestOnClient')

def setup() -> bool:
    global args
    global serviceName

    args = parser.parse_args()
    sat.setLogin(userName, userPasswd)
    
    if not sat.connect():
        return False
    
    serviceName = args.service_chan
    sat.setNewChanCallBack(onChannelAdded)

    return True

def loop() -> bool:
    sat.tick()
    return sat.isConnected()

####################################################
#CALLBACKs

def onChannelAdded(ch: FlowChannel):
    if ch.name == serviceName:
        val = {"test3" : "a string", "test4": True}

        sync = sat.newSyncClient()

        # THIS IS A BLOCKING (sychronous) CALL
        val = sync.sendServiceRequest(ch.chanID, "testCmd", val)

        sync.close()

        if val:
            print("Service response RECEIVED: {}".format(val))

        else:
            print("Service ERROR")
    
####################################################
