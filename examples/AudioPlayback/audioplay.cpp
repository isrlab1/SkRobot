#include "audioplay.h"


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/Containers/skarraycast.h>

ConstructorImpl(AudioPlay, SkFlowSat)
{
    pa = nullptr;
    output = nullptr;
    inputChan = nullptr;

    setObjectName("AudioPlay");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool AudioPlay::onSetup()
{
    SkCli *cli = skApp->appCli();
    device  = cli->value("--device").toString();
    inputChanName = cli->value("--src-chan").toString();

    setLogin("guest", "password");

    return true;
}

void AudioPlay::onInit()
{

}

void AudioPlay::onQuit()
{
    stop();
}

void AudioPlay::onChannelAdded(SkFlowChanID chanID)
{
    if (inputChan)
        return;

    SkFlowChannel *ch = channel(chanID);

    if (ch->name == inputChanName)
    {
        // // //
        SkFlowSync *sync = buildSyncClient();
        sync->setCurrentDbName(ch->name.c_str());

        SkVariant v;

        SkString streamFMT;
        SkString streamCDC;
        uint channels = 0;
        uint bufferFrames = 0;
        uint sampleRate = 0;
        SkAudioFmtType t;

        AssertKiller(!sync->getVariable("stream", v));
        streamFMT = v.toString();

        AssertKiller(!sync->getVariable("codec", v));
        streamCDC = v.toString();

        if (streamFMT != "PCM" || streamCDC != "RAW")
        {
            ObjectError("Stream format/codec NOT supported");
            skApp->quit();
            return;
        }

        AssertKiller(!sync->getVariable("channels", v));
        channels = v.toUInt();

        AssertKiller(!sync->getVariable("bufferFrames", v));
        bufferFrames = v.toUInt();

        AssertKiller(!sync->getVariable("sampleRate", v));
        sampleRate = v.toUInt();

        AssertKiller(!sync->getVariable("fmt", v));
        t = static_cast<SkAudioFmtType>(v.toUInt());

        if (!params.set(true, channels, sampleRate, bufferFrames, t))
        {
            skApp->quit();
            return;
        }

        ulong tickIntervalUS = 0;

        AssertKiller(!sync->getVariable("tickTimePeriod", v));
        tickIntervalUS = v.toFloat()*1000000;

        if (eventLoop()->getFastInterval() > tickIntervalUS || eventLoop()->getTimerMode() != SK_TIMEDLOOP_RT)
        {
            ObjectWarning("Need to CHANGE tick interval: " << eventLoop()->getFastInterval() << " -> " << tickIntervalUS);
            eventLoop()->changeFastZone(tickIntervalUS);
            eventLoop()->changeSlowZone(tickIntervalUS*4);
        }

        sync->close();
        sync->destroyLater();
        // // //

        inputChan = ch;

        if (!play())
            skApp->quit();

        subscribeChannel(inputChan);
    }
}

void AudioPlay::onChannelRemoved(SkFlowChanID chanID)
{
    if (!inputChan)
        return;

    SkFlowChannel *ch = channel(chanID);

    if (chanID == inputChan->chanID)
    {
        stop();
        inputChan = nullptr;
    }
}


void AudioPlay::onFlowDataCome(SkFlowChannelData &data)
{
    if (!output || !inputChan)
        return;

    if (data.chanID == inputChan->chanID)
    {
        SkDataBuffer &d = data.data;
        output->addData(d.data(), d.size());
    }
}

bool AudioPlay::play()
{
    pa = new SkPortAudio(this);
    pa->setObjectName(this, "PortAudio");

    if (!pa->init())
    {
        pa->destroyLater();
        pa = nullptr;
        return false;
    }

    output = pa->getProduction().outputProduction;

    if (!pa->play(device.c_str(), params))
    {
        pa->destroyLater();
        pa = nullptr;
        output = nullptr;
        return false;
    }

    output->setCanonicalSize(params.getCanonicalSize());
    ObjectMessage("Playback STARTED [device: " << device << "]");

    return true;
}

void AudioPlay::stop()
{
    if (!pa)
        return;

    if (pa->isInitialized())
    {
        if (pa->isStreamActive())
            pa->stop();

        pa->close();

        output = nullptr;
    }

    pa->destroyLater();
    pa = nullptr;

    ObjectMessage("Playback STOPPED");
}


