#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat

###############################################################################
# SKETCH

userName = "User1"
userPasswd = "password"
outputChanName = "PyTalker"
canonicalOutputChanName = "User1.PyTalker"

outputChan = None

count = 0

sat = FlowSat()

def setup():
    print("[SETUP] ..")
    
    sat.setLogin(userName, userPasswd)

    t = 0.50 #VERY SLOW, to compare with ROS talker example
    sat.setTickTimer(t, t * 2)

    sat.setNewChanCallBack(onChannelAdded)
    sat.setStartChanPubReqCallBack(onStartChanPub)
    sat.setStopChanPubReqCallBack(onStopChanPub)

    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:
        sat.setSpeedMonitorEnabled(True)
        sat.addStreamingChannel(Flow_T.FT_BLOB, Variant_T.T_STRING, outputChanName, "text/plain")
        print("[LOOP] ..")
    
    return ok

def loop():
    global count

    if outputChan and outputChan.isPublishingEnabled:
        txt = "Hello World: {}".format(count)
        sat.publishString(outputChan.chanID, txt)
        print("Publishing \"{}\"".format(txt))
        count += 1

    sat.tick()
    return sat.isConnected()

###############################################################################
# CALLBACKs

def onChannelAdded(ch):
    global outputChan

    if ch.name == canonicalOutputChanName:
        outputChan = ch
        print("Output channel is READY")

def onStartChanPub(ch):
    global count
    if ch.name == canonicalOutputChanName:
        count = 0
        sat.setSpeedMonitorEnabled(False)
        print("\nRequest to START publish on output channel")

def onStopChanPub(ch):
    if ch.name == canonicalOutputChanName:
        sat.setSpeedMonitorEnabled(True)
        print("\nRequest to STOP publish on output channel")

###############################################################################
