#!/usr/bin/env pysketch-executor

###############################################################################
##
## Copyright (C) Daniele Di Ottavio (aka Tetsuo)
## Contact: tetsuo.tek (at) gmail (dot) com
##
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License
## as published by the Free Software Foundation; either version 3
## of the License, or (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
##
###############################################################################

from PySketch.abstractflow import FlowChannel
from PySketch.flowsync import FlowSync
from PySketch.flowproto import FlowChanID, Variant_T, Flow_T
from PySketch.flowsat import FlowSat
from PySketch.elapsedtime import ElapsedTime

###############################################################################
# GLOBAL

userName = "User1"
userPasswd = "password"

# Subscribing "Pairs" CoreStreamingChannel, we can retrieve pairs changing (SET or DEL) on active databases,
# when happen, as json list of values, whitout wasting network using polling data procedures.
inputChanName = "Pairs"

inputChan = None

sat = FlowSat()

sync = None
checkSyncChrono = ElapsedTime()

secondsUpTime = 0
uptimeChrono = ElapsedTime()

def setup():
    global sync

    print("[SETUP] ..")

    sat.setLogin(userName, userPasswd)

    t = 0.005 # seconds
    sat.setTickTimer(t, t * 50)

    # Setting up callbacks required to get channel-added and scuscribe-data-come events
    sat.setNewChanCallBack(onChannelAdded)
    sat.setGrabDataCallBack(onDataGrabbed)

    # make connction as "User1","password"
    ok = sat.connect() # uses the env-var ROBOT_ADDRESS

    if ok:
        # hide the timing debug bar
        sat.setSpeedMonitorEnabled(False)

        sync = sat.newSyncClient()
        sync.setCurrentDbName(userName) # set current database as target for operation (only setVariable here)

        print("[LOOP] ..")
    
    # sketch will exit on ok == False
    return ok

def loop():
    global secondsUpTime

    sat.tick()

    # check for 5 seconds, to check the service
    if checkSyncChrono.stop() >= 5.0:
        # let synch-client alive; we don't want reconnect each time, when it will be disconnected on timeout with no traffic-data
        sync.checkService()

        #reset check-connection chronometer
        checkSyncChrono.start()

    # check for 1 second, to increment and set uptime pair
    if uptimeChrono.stop() >= 1.0:
        secondsUpTime += 1#  whe count seconds since sketch started

        sync.setVariable("uptime", secondsUpTime) # set pair value on database

        #reset uptime chronometer
        uptimeChrono.start()

    # sketch will exit on connection die
    return sat.isConnected()

###############################################################################
# CALLBACKs

# callback called when new channel is notified from FlowNetwork (here will come also channel added from here)
def onChannelAdded(ch):
    global inputChan

    if ch.name == inputChanName:
        inputChan = ch
        # Subscribing "Pairs" CoreStreamingChannel
        sat.subscribeChannel(inputChan.chanID)

# callback called when subscribing data come here from FlowNetwork (following the sat.tick())
def onDataGrabbed(chanID, data):
    if inputChan is None:
        return
    
    if inputChan.chanID == chanID:
        # Grabbing pair event from "Pairs" CoreStreamingChannel; txt contains a JSON list of 4 items:
        # [0] SET or DEL
        # [1] Pair variable name
        # [2] Pair variable value
        # [3] Pair-database name
        txt = data.decode('utf-8')
        print("-> {}".format(txt))
    
###############################################################################

# AVAILABLE PAIR-DATABASES
#
# * SkRobot creates "Main" database by default; it contains configurations and allowed command-transaction to target on SkRobot-WorkingObject
#   Some important properties:
#       - channels [list] -> all channels open on FlowNetwork
#       - databases [list] -> all database open on FlowNetwork
#
# * SkRobot creates "Connections" database by default; it contains all connections (synch and async) authorized on SkRobot
#   keys are connID, associated value is object wirh connection properties
#
# * Each connected user has a database
#   Some important properties:
#       - asynchs [list] -> all flow (connID) open on FlowNetwork by an user
#       - asynchsCount [integer] -> flow count
#
# * Each active channel has a database: subscribers[list], subscribersCount, targets[list], targetsCount, source, totalPublishedBytes, publishSpeedBytesPerSec
#   Some important properties:
#       - subscribers [list] -> all subscriber-connIDs
#       - subscribersCount [integer] -> the count for subscriber-connIDs
#       - source [string] -> the channel-name that this channel is attached to
#       - targets [list] -> targets attached to this channel
#       - targetsCount [integer] -> the count for targets attached to this channel
#       - totalPublishedBytes [integer] -> data speed average considering data sent from publisher, but also redistribution data to all subscribers
#       - publishSpeedBytesPerSec [integer] - data publication speed average

# USING PAIRS WITH FLOWSYNC CLIENT
#
# AbstractFlow meths (NO RESPONSE):
#   sync.setCurrentDbName(dbName) # DEFAULT IS Main
#   sync.setVariable(key, val)
#   sync.delVariable(key)
#   sync.flushAll()
#
# FlowSync(AbstractFlow) meths (WITH BLOCKING RESPONSE):
#   sync.existsOptionalPairDb(dbName)
#   sync.addDatabase(dbName)
#   sync.getCurrentDbName()
#   sync.variablesKeys()
#   sync.getAllVariables()
#   sync.existsVariable(key)
#   sync.getVariable(key)

# USING PAIRS WITH HTTP/JSON GET CLIENT (NO POSTs) - few commands and less control, useful ONLY for pairs content debug and monitoring
#
# On Main database      
#   GET variables name list                   -> http://127.0.0.1:9001/pairs?db=Main&cmd=keys
#   GET all database content                  -> http://127.0.0.1:9001/pairs?db=Main&cmd=dump
#   GET variable content                      -> http://127.0.0.1:9001/pairs?db=Main&cmd=get&key=tickAvg
#   DEL variable content                      -> http://127.0.0.1:9001/pairs?db=Main&cmd=del&key=tickAvg
#   GET databases list                        -> http://127.0.0.1:9001/pairs?db=Main&cmd=get&key=databases
#   GET channels list                         -> http://127.0.0.1:9001/pairs?db=Main&cmd=get&key=channels
#
# On Connections database
#   GET connections (connID) list             -> http://127.0.0.1:9001/pairs?db=Connections&cmd=keys
#   GET connection (connID) properties object -> http://127.0.0.1:9001/pairs?db=Connections&cmd=get&key=User1@SkLocalSocket(8)
#
# On user database      
#   GET variables name list                   -> http://127.0.0.1:9001/pairs?db=User1&cmd=keys
#
# On channel-database
#   GET total data published on a channel     -> http://127.0.0.1:9001/pairs?db=User1.Camera.MJPEG&cmd=get&key=totalPublishedBytes
#   GET data publication speed for a channel  -> http://127.0.0.1:9001/pairs?db=User1.Camera.MJPEG&cmd=get&key=publishSpeedBytesPerSec
