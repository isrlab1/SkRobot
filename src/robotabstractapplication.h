#ifndef ROBOTABSTRACTAPPLICATION_H
#define ROBOTABSTRACTAPPLICATION_H

#include <Core/App/skapp.h>
#include <Core/Object/skabstractworkerobject.h>
#include <Core/System/Network/FlowNetwork/skflowserver.h>

class RobotAbstractApplication extends SkAbstractWorkerObject
{
    bool enabled;
    bool appWouldExit;

    public:
        void setEnabled(bool enable);
        bool isEnabled();

        Slot(appInit);
        Slot(appExitRequest);
        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);
        Signal(initialized);
        Slot(onGameClosed);

    protected:
        AbstractConstructor(RobotAbstractApplication, SkFlowSkAbstractWorkerObjectServer);

        void quit();

        virtual bool onAppInit()        = 0;
        virtual void onAppExit()        = 0;

        virtual void onFastTick()       = 0;
        virtual void onSlowTick()       = 0;
        virtual void onOneSecTick()     = 0;
};

#endif // ROBOTABSTRACTAPPLICATION_H
