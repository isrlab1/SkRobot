#include "authenticator.h"
#include "physicalbody.h"

#if defined(__linux__)
    #include <shadow.h>
    #include <crypt.h>
#endif

#include <Core/System/Filesystem/skfsutils.h>

ConstructorImpl(Authenticator, SkAbstractFlowAuthenticator)
{
    owner = DynCast(PhysicalBody, parent());
    setObjectName("Authenticator");
}

void Authenticator::setup(CStr *usersDirectory)
{
    usersDirPath = SkFsUtils::adjustPathEndSeparator(usersDirectory);
}

bool Authenticator::onAuthorization(CStr *username, CStr *token, CStr *seed, SkFlowServicePermission *permissions)
{
    // VIRTUAL-ACCOUNT
    if (isVirtualAccount(username))
        return authorizeVirtualLogin(username, token, permissions);

    else
    {
        // DIR-ACCOUNT
        if (checkAuthDir(username))
            return authorizeAccount(username, token, seed, permissions);

        // NOT A VIRTUAL-ACCOUNT AND NOT A NORMAL DIR-ACCOUNT
        else
            //SkRobt MUST RUN AS root (OR OTHER EQUIVALENT) - ONLY LINUX-SHADOW IS SUPPORTED AT NOW
            return authorizeSysAccount(username, token, permissions);

    }

    return true;
}

bool Authenticator::checkAuthDir(CStr *username)
{
    SkString dirPath;
    Stringify(dirPath, usersDirPath << username);

    return (SkFsUtils::exists(dirPath.c_str()) && SkFsUtils::isDir(dirPath.c_str()) && SkFsUtils::isReadable(dirPath.c_str()));
}

bool Authenticator::authorizeAccount(CStr *username, CStr *token, CStr *seed, SkFlowServicePermission *permissions)
{
    SkArgsMap authMap;

    SkString dirPath;
    Stringify(dirPath, usersDirPath << username);

    SkString userPairsFilePath;
    Stringify(userPairsFilePath, SkFsUtils::adjustPathEndSeparator(dirPath.c_str()) << "pairs.json");

    ObjectMessage("Checking normal login: " << userPairsFilePath);

    if (!SkFsUtils::exists(userPairsFilePath.c_str())
        || !SkFsUtils::isFile(userPairsFilePath.c_str())
        || !SkFsUtils::isReadable(userPairsFilePath.c_str())
        || !SkFsUtils::readJSON(userPairsFilePath.c_str(), authMap))
    {
        ObjectError("FsConnection related user-pairs file is NOT valid: " << userPairsFilePath);
        return false;
    }

    SkString hashedToken;
    SkString requestedToken(token);

    if (SkString::isEmpty(seed))
        hashedToken = stringHash(requestedToken, SkHashMode::HM_SHA512);

    else
    {
        SkString lastSeed(seed);
        hashedToken = stringHash(lastSeed, HM_SHA512);
    }

    bool loggedIN = false;

    SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = authMap.iterator();

    while(itr->next())
    {
        SkString &k = itr->item().key();
        SkVariant &v = itr->item().value();

        if (k == "token")
        {
            SkString authToken = v.toString();

            if (SkString::isEmpty(seed))
            {
                if (authToken == hashedToken)
                {
                    loggedIN = true;
                    ObjectWarning("User has LoggedIN with SIMPLE Token: " << username);
                }

                else
                {
                    ObjectError("SIMPLE Token is NOT a valid account: " << username);
                    delete itr;
                    return false;
                }
            }

            else
            {
                //cout << hashedToken << "\n";
                //cout << authToken << "\n";
                hashedToken.append(authToken);
                hashedToken = stringHash(hashedToken, HM_SHA512);
                //cout << hashedToken << "\n";

                if (requestedToken == hashedToken)
                {
                    loggedIN = true;
                    ObjectWarning("User has LoggedIN with HASHED Token: " << username);
                }

                else
                {
                    ObjectError("HASHED Token is NOT a valid account: " << username);
                    delete itr;
                    return false;
                }
            }
        }

        else if (k == "isOsUser")
            permissions->isOsUser = false;

        else if (k == "isAdmin")
            permissions->isAdmin = v.toBool();

        else if (k == "canSaveDatabases")
            permissions->canSaveDatabases = v.toBool();

        else
            ObjectWarning("UNKNOWN user pair-parameter: " << k);
    }

    delete itr;

    return loggedIN;
}

bool Authenticator::authorizeSysAccount(CStr *userName, CStr *token, SkFlowServicePermission *permissions)
{
#if defined(__linux__)

    struct spwd *shadowEntry = getspnam(userName);

    if (shadowEntry)
    {
        ObjectMessage("Checking os login: " << userName);

        SkOsUser user;

        if (!SkOsEnv::userInfos(userName, user))
        {
            ObjectError("FAILED while getting OS user properties : " << userName);
            return false;
        }

        ObjectMessage("Retrieved OS user properties -> "
                      << "UID: " << user.uid << "; GID: " << user.gid << "; "
                      << "Home: " << user.home << "; Shell: " << user.shell);

        char *encryptedPassword = crypt(token, shadowEntry->sp_pwdp);


        if (!SkString::compare(encryptedPassword, shadowEntry->sp_pwdp))
        {
            ObjectError("FsConnection seems to be related to an OS user account, but login has FAILED: " << userName);
            return false;
        }

        SkOsUser currUser;
        SkOsEnv::userInfos(osEnv()->getCurrentUID(), user);

        permissions->isOsUser = true;
        permissions->isAdmin = (currUser.uid == user.uid);
        permissions->canSaveDatabases = true;

        return true;
    }
#endif

    return false;
}

/*
int main()
{
    //char salt[21];
    //sprintf(salt, "$6$%s$", "Urb3z/pb");
    //printf("%s\n", crypt("password", (char*) salt));
}*/

/*
mark:$6$.n.:17736:0:99999:7:::
[--] [----] [---] - [---] ----
|      |      |   |   |   |||+-----------> 9. Unused
|      |      |   |   |   ||+------------> 8. Expiration date
|      |      |   |   |   |+-------------> 7. Inactivity period
|      |      |   |   |   +--------------> 6. Warning period
|      |      |   |   +------------------> 5. Maximum password age
|      |      |   +----------------------> 4. Minimum password age
|      |      +--------------------------> 3. Last password change
|      +---------------------------------> 2. Encrypted Password
+----------------------------------------> 1. Username
Username (Nome utente). La stringa digitata quando si accede al sistema.
 L'account utente che esistente sul sistema.

Encrypted Password (Password crittografata). La password utilizza il formato
 $type$salt$hashed.

 $type è l'algoritmo di crittografia hash del metodo e può avere i seguenti valori:
    $1$ - MD5
    $2a$ - Blowfish
    $2y$ - Eksblowfish
    $5$ - SHA-256
    $6$ - SHA-512
*/
