#ifndef CONFIG_H
#define CONFIG_H

#include <Core/Object/skabstractworkerobject.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class PhysicalBody;

class Config extends SkWorkerCommand
{
    PhysicalBody *owner;
    SkString hashCfg;

    public:
        Config();

        bool init(PhysicalBody *physicalBody);
        bool setup(SkArgsMap &cfg);
        void save();

        bool checkCfgFile();

        CStr *hash();

    private:
        bool checkCfgPair();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // CONFIG_H
