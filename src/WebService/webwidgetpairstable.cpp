#include "webwidgetpairstable.h"

ConstructorImpl(WebWidgetPairsTable, WebWidgetContainer)
{
    pairs = nullptr;

    t = WebWidgetType::WT_CONTAINER;
}

void WebWidgetPairsTable::setPair(CStr *pairDbName, CStr *key, CStr *udm)
{
    dbName = pairDbName;
    k = key;

    if (udm)
        u = udm;
}

void WebWidgetPairsTable::build(SkArgsMap &pairsMap)
{
    pairs = new WebWidgetContainer;
}

CStr *WebWidgetPairsTable::pairsDatabaseName()
{
    return dbName.c_str();
}

CStr *WebWidgetPairsTable::pairKey()
{
    return k.c_str();
}
