#ifndef WEBWIDGETFORM_H
#define WEBWIDGETFORM_H

#include "webwidgetcontainer.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class WebWidgetForm extends WebWidgetContainer
{
    public:
        Constructor(WebWidgetForm, WebWidgetContainer);

        void setPair(CStr *pairDbName, CStr *pairKey);
        void build(SkArgsMap &formMap);

        CStr *pairsDatabaseName();
        CStr *pairKey();

    private:
        SkString k;
        SkString dbName;
        SkArgsMap lastFormMap;
        SkString lastFormMapHash;

        WebWidgetContainer *form;
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // WEBWIDGETFORM_H
