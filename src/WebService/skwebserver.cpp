#include "skwebserver.h"
#include "webfrontendmountpoint.h"
#include "agentmountpoint.h"

#include <Core/App/skapp.h>
#include <Core/System/Network/TCP/HTTP/Server/skhttpservice.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skfsmountpoint.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skpartredistrmountpoint.h>
#include <Core/System/Network/TCP/HTTP/Server/Mountpoints/skwsredistrmountpoint.h>
#include <Core/System/Network/TCP/HTTP/skwebsocket.h>
#include "Core/System/skdeviceredistr.h"
#include <Core/Containers/skarraycast.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkWebServerCfg, SkAbstractModuleCfg)
{}

void SkWebServerCfg::allowedCfgKeysSetup()
{
    SkWorkerParam *p = nullptr;

    addParam("listenAddr", "Listen address", T_STRING, true, "0.0.0.0", "Sets WebServer listen address");

    p = addParam("listenPort", "Tcp port", T_UINT16, true, UShort(9999), "Sets WebServer listen tcp-port");
    p->setMin(1);
    p->setMax(65535);
    p->setStep(1);

    addParam("maxHeaderSize", "Max http header size", T_UINT64, true, ULong(100000), "Sets maximum bytes length accepted for http requests");

    p = addParam("maxConnQueued", "max queued", T_UINT16, true, UShort(100), "Sets max connection on WebServer accepting queue");
    p->setMin(1);
    p->setMax(65535);
    p->setStep(1);

    addParam("enableSsl", "Enables SSL", T_BOOL, true, false, "Enables/Disables the ssl protocol on WebServer");
    addParam("sslKeyFile", "SSL key", T_STRING, true, "ssl/selfsigned.key", "Sets filepath for the ssl-key");
    addParam("sslCertFile", "SSL certificate", T_STRING, true, "ssl/selfsigned.crt", "Sets filepath for the ssl-certificate");

    addParam("rootDir", "Root contents directory", T_STRING, true, "www", "Sets rootpath for http static contents");

    addParam("idleTimeoutInterval", "IDLE timeout", T_UINT16, true, UShort(20), "Sets max seconds interval to kill an idle http connection");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(SkWebServer, SkAbstractModule)
{
    sync = nullptr;

    service = nullptr;
    wwwMp = nullptr;
    homeAppMp = nullptr;
    pairsMp = nullptr;
    frontendMp = nullptr;
    //agentMp = nullptr;

    SlotSet(onPairsPageAccepted);
    SlotSet(onStreamTargetAdded);
    SlotSet(onStreamTargetRemoved);
}

bool SkWebServer::onSetup()
{
    return true;
}

void SkWebServer::onStart()
{}

void SkWebServer::onStop()
{}

void SkWebServer::onSetEnabled(bool enabled)
{
    SkAbstractModuleCfg *cfg = config();

    if (enabled)
    {
        sync = buildSyncClient();

        if (!sync)
        {
            requestToEnable(false);
            return;
        }

        sync->setCurrentDbName("Main");
        SkVariant v;
        sync->getVariable("appName", v);
        svrName = v.toString();

        SkString wwwPath = cfg->getParameter("rootDir")->value().toString();
        wwwPath = SkFsUtils::adjustPathEndSeparator(wwwPath.c_str());

        if (!SkFsUtils::exists(wwwPath.c_str()) || !SkFsUtils::isDir(wwwPath.c_str()) || !SkFsUtils::isReadable(wwwPath.c_str()))
        {
            ObjectError("Web RootDirectory is NOT valid: " << wwwPath);
            requestToEnable(false);
            return;
        }

        bool ssl = cfg->getParameter("enableSsl")->value().toBool();
        SkString sslCertificate;
        SkString sslKey;

        if (ssl)
        {
            sslCertificate = cfg->getParameter("sslCertFile")->value().toString();

            if (sslCertificate.isEmpty())
            {
                ObjectError("SSL Certificate CANNOT be EMPTY");
                requestToEnable(false);
                return;
            }

            else if (!SkFsUtils::exists(sslCertificate.c_str()) || !SkFsUtils::isFile(sslCertificate.c_str()) || !SkFsUtils::isReadable(sslCertificate.c_str()))
            {
                ObjectError("SSL Certificate file is NOT valid");
                requestToEnable(false);
                return;
            }

            sslKey = cfg->getParameter("sslKeyFile")->value().toString();

            if (sslKey.isEmpty())
            {
                ObjectError("SSL key CANNOT be EMPTY");
                requestToEnable(false);
                return;
            }

            else if (!SkFsUtils::exists(sslKey.c_str()) || !SkFsUtils::isFile(sslKey.c_str()) || !SkFsUtils::isReadable(sslKey.c_str()))
            {
                ObjectError("SSL key file is NOT valid");
                requestToEnable(false);
                return;
            }
        }

        service = new SkHttpService(this);
        service->setObjectName(this, "Service");
        service->setup(objectName(),
                       ssl,
                       cfg->getParameter("maxHeaderSize")->value().toUInt64(),
                       cfg->getParameter("idleTimeoutInterval")->value().toUInt16());

        if (ssl)
            service->sslSvr()->initSslCtx(sslCertificate.c_str(), sslKey.c_str());

        wwwMp = new SkFsMountPoint(service);

        if (!wwwMp->setup("/", true) || !wwwMp->init(wwwPath.c_str()) || !service->addMountPoint(wwwMp))
        {
            requestToEnable(false);
            return;
        }

        frontendMp = new WebFrontendMountpoint(service);

        if (!frontendMp->setup("/frontend-controller", true) || !frontendMp->init(svrName.c_str(), sync) || !service->addMountPoint(frontendMp))
        {
            requestToEnable(false);
            return;
        }

        /*agentMp = new WebAgentMountpoint(service);

        if (!agentMp->setup("/agent", true) || !agentMp->init(svrName.c_str(), cfg->getFsPath(), objectName(), token.c_str()) || !service->addMountPoint(agentMp))
        {
            requestToEnable(false);
            return;
        }*/

        homeAppMp = new SkGenericMountPoint(service);
        homeAppMp->setPathAsExpandable(true);

        if (!homeAppMp->setup("/app", true) || !service->addMountPoint(homeAppMp))
        {
            requestToEnable(false);
            return;
        }

        Attach(homeAppMp, accepted, frontendMp, appPageAccepted, SkDirect);

        pairsMp = new SkGenericMountPoint(service);

        if (!pairsMp->setup("/pairs", true) || !service->addMountPoint(pairsMp))
        {
            requestToEnable(false);
            return;
        }

        Attach(pairsMp, accepted, this, onPairsPageAccepted, SkDirect);

        SkString listenAddress = cfg->getParameter("listenAddr")->value().toString();
        UShort listenPort = cfg->getParameter("listenPort")->value().toUInt16();
        UShort maxQueuedConnections = cfg->getParameter("maxConnQueued")->value().toUInt16();

        if (!service->start(listenAddress.c_str(), listenPort, maxQueuedConnections))
        {
            requestToEnable(false);
            return;
        }

        SkBinaryTreeVisit<SkFlowChanID, SkFlowChannel *> *itr = channelsIterator();

        while(itr->next())
        {
            SkFlowChanID chanID = itr->item().key();
            SkFlowChannel *ch = itr->item().value();

            if (ch->chan_t != StreamingChannel)
                continue;

            onChannelAdded(chanID);
        }

        delete itr;

        eventLoop()->changeFastZone(skApp->getFastInterval()/2);
        eventLoop()->changeSlowZone(skApp->getFastInterval()*8);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_RT);

        if (ssl)
            ObjectMessage("Service OPEN (with SSL support): " << listenAddress << ":" << listenPort);
        else
            ObjectMessage("Service OPEN (without SSL support): " << listenAddress << ":" << listenPort);
    }

    else
    {
        frontendMp->close();
        frontendMp = nullptr;

        homeAppMp = nullptr;
        wwwMp = nullptr;
        pairsMp = nullptr;
        frontendMp = nullptr;

        if (sync)
        {
            sync->close();
            sync->destroyLater();
            sync = nullptr;
        }

        service->stop();
        service->destroyLater();
        service = nullptr;

        SkBinaryTreeVisit<SkFlowChanID, SkWebServerFlowRedistrMP *> *itr = redistrs.iterator();

        while(itr->next())
        {
            SkWebServerFlowRedistrMP *r = itr->item().value();

            if (isSubscribed(r->chanID))
                unsubscribeChannel(r->chanID);

            delete r;
        }

        delete itr;
        redistrs.clear();

        eventLoop()->changeFastZone(250000);
        eventLoop()->changeSlowZone(250000);
        eventLoop()->changeTimerMode(SK_TIMEDLOOP_SLEEPING);

        ObjectMessage("Service CLOSED");
    }
}

void SkWebServer::onFastTick()
{

}

void SkWebServer::onSlowTick()
{

}

void SkWebServer::onOneSecTick()
{

}

void SkWebServer::onChannelAdded(SkFlowChanID chanID)
{
    if (!isEnabled())
        return;

    SkFlowChannel *ch = channel(chanID);

    if (!ch || ch->chan_t == ServiceChannel)
        return;

    if (isInternalChannel(ch->name.c_str()))
    {
        subscribeChannel(ch->name.c_str(), chanID);
        return;
    }

    ObjectMessage("StreamingChannel ADDED: " << ch->name << " [ChanID: " << ch->chanID << "]");

    SkWebServerFlowRedistrMP *r = new SkWebServerFlowRedistrMP;

    //HTTP section will be destroyed from http-service through the direct signal trigger removedChannel(..)
    r->mpPath = "/";
    r->mpPath.append(svrName);
    r->mpPath.append("/");
    r->mpPath.append(ch->name);

    SkRedistrMountPoint *redistrMp = nullptr;

    if (ch->mime == SkMimeType::getMimeType("jpeg"))
    {
        r->mpPath.append(".multipart");

        r->rawRedistr = nullptr;
        r->partRedistr = new SkPartRedistrMountPoint(this);

        if (!r->partRedistr->setup(r->mpPath.c_str(), true)
            || !r->partRedistr->init(svrName.c_str(), ch->mime.c_str()))
        {
            r->partRedistr->destroyLater();
            delete r;
            return;
        }

        redistrMp = r->partRedistr;
    }

    else
    {
        r->mpPath.append(".raw");

        r->partRedistr = nullptr;
        r->rawRedistr = new SkRawRedistrMountPoint(this);

        SkDataBuffer *hdr = nullptr;

        if (ch->hasHeader)
            hdr = &ch->header;

        if (!r->rawRedistr->setup(r->mpPath.c_str(), true)
            || !r->rawRedistr->init(svrName.c_str(), ch->mime.c_str(), hdr))
        {
            r->rawRedistr->destroyLater();
            delete r;
            return;
        }

        redistrMp = r->rawRedistr;
    }

    SkDeviceRedistr *redistr = redistrMp->getRedistr();
    redistr->setProperty("ChanID", chanID);
    redistr->setProperty("ChanName", ch->name.c_str());

    Attach(redistr, targetAdded, this, onStreamTargetAdded, SkQueued);
    Attach(redistr, targetDeleted, this, onStreamTargetRemoved, SkQueued);

    redistrs[chanID] = r;
    service->addMountPoint(redistrMp);
}

void SkWebServer::onChannelRemoved(SkFlowChanID chanID)
{
    if (!isEnabled() || !redistrs.contains(chanID))
        return;

    SkWebServerFlowRedistrMP *r = redistrs[chanID];
    redistrs.remove(chanID);

    service->delMountPoint(r->mpPath.c_str());
    ObjectMessage("Channel REMOVED: " << chanID << " [" << r->mpPath << "]");

    delete r;
}

void SkWebServer::onChannelHeaderSetup(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    SkWebServerFlowRedistrMP *r = redistrs[chanID];

    if (r->rawRedistr)
    {
        SkDataBuffer &b = r->rawRedistr->getRedistr()->getHeader();
        ULong sz = b.size();
        r->rawRedistr->setHeader(&ch->header);
        ObjectMessage("Channel HEADER modified [" << sz << " -> " << r->rawRedistr->getRedistr()->getHeader().size() << " B]: " << ch->name);
    }
}

void SkWebServer::onChannelPublishStartRequest(SkFlowChanID)
{}

void SkWebServer::onChannelPublishStopRequest(SkFlowChanID)
{}

SlotImpl(SkWebServer, onStreamTargetAdded)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);

    SkFlowChanID chanID = r->property("ChanID").toUInt16();
    SkString chanName = r->property("ChanName").toString();

    if (r->count() == 1/* && !isInternalChannel(chanName.c_str())*/)//  chanName != "Pairs")
    {
        ObjectWarning("Enabling redistr [chanID: " << chanID << "; name: " << chanName << "]: " << redistrs[chanID]->mpPath);
        subscribeChannel(chanName.c_str(), chanID);
    }
}

SlotImpl(SkWebServer, onStreamTargetRemoved)
{
    SilentSlotArgsWarning();

    SkDeviceRedistr *r = dynamic_cast<SkDeviceRedistr *>(referer);

    if (!r)
    {
        ObjectError("Redistr object is NULL");
        return;
    }

    SkFlowChanID chanID = r->property("ChanID").toUInt16();
    SkString chanName = r->property("ChanName").toString();

    if (r->count() == 0 /*&& !isInternalChannel(chanName.c_str())*/)// chanName != "Pairs")
    {
        ObjectWarning("Disabling redistr [chanID: " << chanID << "; name: " << chanName << "]: " << redistrs[chanID]->mpPath);
        unsubscribeChannel(chanID);
    }
}

bool SkWebServer::isInternalChannel(CStr *chanName)
{
    return (SkString::compare(chanName, "Pairs")
            /*|| SkString::compare(chanName, agentSpeakingPcmChanName.c_str())*/);
}

void SkWebServer::onFlowDataCome(SkFlowChannelData &chData)
{
    if (!isEnabled())
        return;

    SkFlowChannel *ch = channel(chData.chanID);

    if (!ch)
        return;

    if (redistrs.contains(chData.chanID))
    {
        SkWebServerFlowRedistrMP *r = redistrs[chData.chanID];

        if (r->partRedistr)
            r->partRedistr->send(chData.data.data(), chData.data.size());

        else if(r->rawRedistr)
            r->rawRedistr->send(chData.data.data(), chData.data.size());

        return;
    }

    //

    if (ch->name == "Pairs")
    {
        SkString json;
        json.append(chData.data.data(), chData.data.size());

        SkVariant v;

        if (!v.fromJson(SkArrayCast::toCStr(json.c_str())))
        {
            ObjectError("Pair json is NOT valid: " << json);
            return;
        }

        SkVariantVector l;
        v.copyToList(l);

        SkString cmd = l.first().toString();
        SkString dbName = l.last().toString();
        SkString key = l[1].toString();
        SkVariant &val = l[2];

        frontendMp->dbPairChanged(cmd.c_str(), dbName.c_str(), key.c_str(), val);
    }

    /*else if (ch->name == agentSpeakingPcmChanName)
        agentMp->setSpeakerPcmChunk(data, sz);*/
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  service pages

SlotImpl(SkWebServer, onPairsPageAccepted)
{
    SilentSlotArgsWarning();

    SkHttpSocket *httpSck = dynamic_cast<SkHttpSocket *>(referer);

    if (!httpSck->url().getQueryMap().contains("db"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkString dbName = httpSck->url().getQueryMap()["db"].toString();

    if (dbName != "Main" && !sync->existsOptionalPairDb(dbName.c_str()))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
        return;
    }

    sync->setCurrentDbName(dbName.c_str());

    if (!httpSck->url().getQueryMap().contains("cmd"))
    {
        service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
        return;
    }

    SkString cmd = httpSck->url().getQueryMap()["cmd"].toString();

    SkString json;

    if (cmd == "keys")
    {
        SkStringList keys;
        sync->variablesKeys(keys);

        SkVariant v = keys;

        if (!v.toJson(json, true))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
            return;
        }
    }

    else
    {
        SkString key;

        if (!httpSck->url().getQueryMap().contains("key"))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::BadHttpRequest);
            return;
        }

        key = httpSck->url().getQueryMap()["key"].toString();

        if (!sync->existsVariable(key.c_str()))
        {
            service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::NotFound);
            return;
        }

        if (cmd == "get")
        {
            SkVariant v;

            if (!sync->getVariable(key.c_str(), v) || !v.toJson(json, true))
            {
                service->sendStatusPageAndKill(httpSck, SkHttpStatusCode::InternalServerError);
                return;
            }
        }

        else if (cmd == "del")
        {
            sync->delVariable(key.c_str());

            httpSck->send(SkHttpStatusCode::Ok);
            return;
        }
    }

    httpSck->send(json, "application/json", SkHttpStatusCode::Ok);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
