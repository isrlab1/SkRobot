#ifndef ABSTRACTSERVICE_H
#define ABSTRACTSERVICE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include <Core/Object/skobject.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class PhysicalBody;
class Config;

class AbstractService extends SkObject
{
    public:
        bool init(PhysicalBody *workerOwner, Config *config);
        void quit();

        bool isRunning();

        Signal(enabled);
        Signal(disabled);
        Signal(errorOccurred);

        Slot(fastTick);
        Slot(slowTick);
        Slot(oneSecTick);

    protected:
        PhysicalBody *owner;
        Config *cfg;
        bool running;

        AbstractConstructor(AbstractService, SkObject);

        virtual bool onInit()                       =0;
        virtual void onQuit()                       =0;

        virtual void onFastTick()                   {}
        virtual void onSlowTick()                   {}
        virtual void onOneSecTick()                 {}
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //


#endif // ABSTRACTSERVICE_H
