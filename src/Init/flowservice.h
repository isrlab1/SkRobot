#ifndef FLOWSERVICE_H
#define FLOWSERVICE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "abstractservice.h"

#include "Core/System/Network/FlowNetwork/skflowserver.h"

#if defined(ENABLE_HTTP)
    #include "Core/System/Network/FlowNetwork/HTTP/skflowhttpservice.h"
#endif

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class FlowService extends AbstractService
{
    SkFlowServer *fs;

#if defined(ENABLE_HTTP)
    SkFlowHttpService *fsHttp;
#endif

    SkFlowPairsDatabase *mainDB;

    SkFlowChanID isoClockCh;
    SkFlowChanID tickCh;
    SkFlowChanID logCh;
    SkFlowChanID evtCh;

    SkQueue<SkLog *> logs;

    public:
        Constructor(FlowService, AbstractService);

        void prepareToQuit();

        SkFlowServer *svr();
        SkFlowPairsDatabase *main();

        SkFlowChanID clockChannel();
        SkFlowChanID tickChannel();
        SkFlowChanID logChannel();
        SkFlowChanID eventChannel();

        Slot(onAppTickChange);

    private:
        bool onInit()                                   override;
        void onQuit()                                   override;

        void onFastTick()                               override;
        void onSlowTick()                               override;
        void onOneSecTick()                             override;

        void onEvent(SkDeliveringEvent *event)          override;

        bool loadConfig();
        void sendAppLogs();
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // FLOWSERVICE_H
