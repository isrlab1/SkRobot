#ifndef MODULESCTRLSERVICE_H
#define MODULESCTRLSERVICE_H

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#include "abstractservice.h"

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

class SkModulesManager;
class FlowService;
class SkModuleThread;
class SkAbstractModuleCfg;

class ModulesCtrlService extends AbstractService
{
    FlowService *flowService;
    SkString token;
    SkArgsMap mods;
    SkModulesManager *modsManager;
    uint64_t quittedModules;

    public:
        Constructor(ModulesCtrlService, AbstractService);

        void setup(FlowService *fs, CStr *superToken);

        SkModulesManager *manager();
        SkArgsMap &modules();

        Slot(onModsManagerStart);
        Slot(onModsManagerStop);
        Slot(onModThreadInstanced);
        Slot(onModTickAvgCheck);
        Slot(onModuleQuitted);

        Slot(onAppTickAvgChange);
        Slot(onAppTickAdjust);

    private:
        bool onInit()                                   override;
        void onQuit()                                   override;

        //DEPRECATED
        //void onModCfgNotFound(SkAbstractModuleCfg *modCfg, SkArgsMap &defaultCfg);
};

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

#endif // MODULESCTRLSERVICE_H
