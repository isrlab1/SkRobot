#include "flowservice.h"
#include "../physicalbody.h"

#include <Core/App/skapp.h>
#include <Core/System/Filesystem/skfsutils.h>

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

ConstructorImpl(FlowService, AbstractService)
{
    fs = nullptr;

#if defined(ENABLE_HTTP)
    fsHttp = nullptr;
#endif

    mainDB = nullptr;

    isoClockCh = -1;
    tickCh = -1;
    logCh = -1;
    evtCh = -1;

    setObjectName("FlowService");

    SlotSet(onAppTickChange);

    Attach(skApp->changed_SIG, pulse, this, onAppTickChange, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool FlowService::loadConfig()
{
    ObjectWarning("Loading application config ..");

    if (cfg->parameters().isEmpty() && !cfg->init(owner))
        return false;

    SkString workingDir = cfg->value("workingDir").toString();

    if (workingDir.isEmpty())
        ObjectMessage("Working directory: ./");

    else
        ObjectMessage("Working directory: " << workingDir);

    if (!workingDir.isEmpty() && !SkFsUtils::chdir(workingDir.c_str()))
    {
        ObjectError("Working directory is NOT valid: " << workingDir);
        return false;
    }

    SkString appName = cfg->value("appName").toString();

    if (appName.isEmpty())
        owner->setObjectName("Robot");

    else
        owner->setObjectName(appName.c_str());

    mainDB->setVariable("appName", appName);

    SkString sharedAddress = cfg->value("sharedAddress").toString();

    if (sharedAddress.isEmpty())
    {
        ObjectWarning("Robot setup has NOT public SHARED_ADDRESS");

        if (mainDB->existsVariable("sharedAddress"))
            mainDB->delVariable("sharedAddress");
    }

    else
    {
        ObjectMessage("Robot setup has public SHARED_ADDRESS: " << sharedAddress);
        skApp->setGlobalParameter("SHARED_ADDRESS", sharedAddress);
        mainDB->setVariable("sharedAddress", sharedAddress);
    }

    return true;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

bool FlowService::onInit()
{
    if (fs)
    {
        fs->close();
        fs->destroyLater();
        fs = nullptr;
    }

    fs = new SkFlowServer(owner);
    fs->setObjectName("FlowServer");

    AssertKiller(!fs->init(owner->objectName(), owner, "database/", "home/"));

    mainDB = fs->getPairDatabase(nullptr);

    SkCli *cli = skApp->appCli();

    if (cli->isUsed("--set-working-dir"))
    {
        SkString workingDir = cli->value("--set-working-dir").toString();

        if (!workingDir.isEmpty() && !SkFsUtils::chdir(workingDir.c_str()))
        {
            ObjectError("Working directory is NOT valid: " << workingDir);
            return false;
        }
    }

    SkString dbFilePath("database/Main.variant");

    if (!SkFsUtils::exists(dbFilePath.c_str()))
        mainDB->save(dbFilePath.c_str());

    if (!mainDB || !mainDB->load(dbFilePath.c_str()) || !loadConfig())
        return false;

#if defined(ENABLE_REDIS)
    if (cfg->value("enableRedis").toBool()
        && !fs->enableRedis(cfg->value("redisAddr").data(), cfg->value("redisPort").toUInt16()))
    {
        return false;
    }
#endif

    SkString localDeliveryPath(osEnv()->getSystemTempPath());
    localDeliveryPath.append(owner->objectName());

    if (!fs->initLocalServer(localDeliveryPath.c_str(), 100))
        return false;

    /*SkString listenAddr;

    if (skApp->existsGlobalParameter("SHARED_ADDRESS"))
        listenAddr = skApp->getGlobalParameter("SHARED_ADDRESS").toString();*/

    if (cfg->value("enableTcpFs").toBool())
    {
        SkString addr;

        //if (listenAddr.isEmpty())
        {
            addr = cfg->value("fsListenAddr").toString();
            ObjectWarning("Robot setup has NOT public SHARED_ADDRESS, setting up FsService on configured 'fsListenAddr': " << addr);

        }

        /*else
        {
            addr = listenAddr;
            ObjectMessage("Setting up FsService listen-address on configured SHARED_ADDRESS: " << addr);
        }*/

        if (!fs->initTcpServer(addr.c_str(), cfg->value("fsListenPort").toUInt16(), cfg->value("fsMaxConnQueued").toUInt16()))
            return false;
    }

#if defined(ENABLE_HTTP)
    fsHttp = new SkFlowHttpService(fs);

    SkString listenAddr;

    //if (listenAddr.isEmpty())
    {
        listenAddr = cfg->value("httpListenAddr").toString();
        ObjectWarning("Robot setup has NOT public SHARED_ADDRESS, setting up FsHttpService on configured 'httpListenAddr': " << listenAddr);
    }

    /*else
    {
        listenAddr = skApp->getGlobalParameter("SHARED_ADDRESS").toString();
        ObjectMessage("Setting up FsHttpService listen-address on configured SHARED_ADDRESS: " << listenAddr);
    }*/

    fsHttp->listenTo(cfg->value("httpListenPort").toUInt16(), listenAddr.c_str());
    fsHttp->enableStaticWWW(cfg->value("httpDir").data());

    if (cfg->value("httpSslEnabled").toBool())
        fsHttp->enableSsl(cfg->value("httpSslCertFile").data(), cfg->value("httpSslKeyFile").data());

    if (!fsHttp->init(fs,
                      cfg->value("httpListenPort").toUInt16(),
                      cfg->value("httpIdleTimeoutInterval").toUInt16(),
                      cfg->value("httpMaxHeaderSize").toUInt64()))
    {
        return false;
    }
#endif

    tickCh = fs->addStreamingChannel(FT_TICK, T_NULL, "Tick");
    logCh = fs->addStreamingChannel(FT_LOGS, T_MAP, "Log", "application/json");
    evtCh = fs->addStreamingChannel(FT_EVENTS, T_MAP, "Events", "application/json");
    isoClockCh = fs->addStreamingChannel(FT_DATETIME_CLOCK, T_STRING, "DateTime.ISO", "application/text");

    Authenticator *authenticator = owner->authenticator();
    fs->setAuthenticator(authenticator);

    //logger->setAsLogQueue(&logs);
    skApp->setupSharedComponent(owner->objectName(), owner);

    SkArgsMap mods;

    SkString modsCfgFile = cfg->value("modsCfgFile").toString();
    AssertKiller(!modsCfgFile.isEmpty() && !mods.fromFile(modsCfgFile.c_str()));

    CStr *superToken = owner->serviceInit()->superToken();

    if (mods.isEmpty())
        authenticator->addVirtualAccount("Dummy", superToken);

    else
    {
        SkAbstractListIterator<SkPair<SkString, SkVariant>> *itr = mods.iterator();

        while(itr->next())
            authenticator->addVirtualAccount(itr->item().key().c_str(), superToken);

        delete itr;
    }

    onAppTickChange();

    enabled();
    return true;
}

void FlowService::onQuit()
{
#if defined(ENABLE_HTTP)
    if (fsHttp)
    {
        fsHttp->quit();
        fsHttp = nullptr;
    }
#endif

    fs->close();
    owner->authenticator()->clear();

    mainDB = nullptr;
    isoClockCh = -1;
    tickCh = -1;
    logCh = -1;
    evtCh = -1;

    skApp->removeSharedComponent(owner->objectName());
    disabled();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void FlowService::onFastTick()
{
    if (tickCh == -1)
        return;

    SkFlowRedistrChannel *ch =  dynamic_cast<SkFlowRedistrChannel *>(fs->getChannelByID(tickCh));

    if (ch->hasSubscribers() || ch->hasGrabbers())
        ch->publish();
}

void FlowService::onSlowTick()
{
}

void FlowService::onOneSecTick()
{
    //sendAppLogs();

    if (isoClockCh == -1)
        return;

    SkFlowRedistrChannel *ch =  dynamic_cast<SkFlowRedistrChannel *>(fs->getChannelByID(isoClockCh));

    if (ch->hasSubscribers() || ch->hasGrabbers())
    {
        SkString dt = SkDateTime::currentDateTimeISOString();
        ch->publish(dt);
    }
}


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void FlowService::onEvent(SkDeliveringEvent *event)
{
    SkArgsMap evtMap;
    evtMap["loop"] = event->owner->eventLoop()->objectName();
    evtMap["owner"] = event->owner->objectName();
    evtMap["owner_T"] = event->owner->typeName();
    evtMap["family"] = event->evtFamily;
    evtMap["name"] = event->evtName;

    if (!event->values.isEmpty())
        evtMap["values"] = event->values;

    SkString json;
    evtMap.toString(json);

    if (mainDB)
    {
        ObjectWarning("Event notify -> " << json);

        if (evtCh == -1)
        {
            ObjectError("Event notify channel NOT ready");
            return;
        }

        SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(fs->getChannelByID(evtCh));

        if (ch->hasSubscribers() || ch->hasGrabbers())
            ch->publish(evtMap);
    }

    else
        ObjectWarning("Event-channel notify DISCARDED -> " << json);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  Logger stream

void FlowService::sendAppLogs()
{
    if (logCh == -1)
        return;

    bool isSubscribed = (fs->getSubscribersCount(logCh) > 0);

    if (isSubscribed)
    {
        uint64_t count = logs.count();
        SkLog **tempLogs = nullptr;

        logger->getMutexEx().lock();

        if (!logs.isEmpty())
        {
            tempLogs = new SkLog * [count];

            for(uint64_t i=0; i<count; i++)
                tempLogs[i] = logs.dequeue();
        }

        logger->getMutexEx().unlock();

        if (tempLogs)
        {
            SkFlowRedistrChannel *ch = dynamic_cast<SkFlowRedistrChannel *>(fs->getChannelByID(logCh));

            if (ch->hasSubscribers() || ch->hasGrabbers())
            {

                for(uint64_t i=0; i<count; i++)
                {
                    SkLog *l = tempLogs[i];
                    SkArgsMap m;

                    m["dateTime"] = l->dateTime.c_str();
                    m["type"] = SkLogMachine::logTypeName(l->t);
                    //m["srcFileName"] = l->srcFileName.c_str();
                    //m["prettyName"] = l->prettyName.c_str();
                    m["descr"] = l->descr.c_str();

                    ch->publish(m);

                    delete l;
                }
            }

            delete tempLogs;
        }
    }

    else
    {
        logger->getMutexEx().lock();
        while(!logs.isEmpty())
            delete logs.dequeue();
        logger->getMutexEx().unlock();
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(FlowService, onAppTickChange)
{
    SilentSlotArgsWarning();

    if (!isRunning())
        return;

    mainDB->setVariable("fastTickTime", skApp->getFastInterval());
    mainDB->setVariable("slowTickTime", skApp->getSlowInterval());
    mainDB->setVariable("tickMode", skApp->getTimerMode());
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void FlowService::prepareToQuit()
{
    ObjectWarning("Preparing to quit ..");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SkFlowServer *FlowService::svr()
{
    return fs;
}

SkFlowPairsDatabase *FlowService::main()
{
    return mainDB;
}

SkFlowChanID FlowService::clockChannel()
{
    return isoClockCh;
}

SkFlowChanID FlowService::tickChannel()
{
    return tickCh;
}

SkFlowChanID FlowService::logChannel()
{
    return logCh;
}

SkFlowChanID FlowService::eventChannel()
{
    return evtCh;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
