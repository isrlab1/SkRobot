#ifndef USERS_D_H
#define USERS_D_H

const char *users_d_rootdir = "users.d";
const unsigned users_d_count = 32;

const struct {
	const bool isDir;
	const char *relativePath;
	const char *name;
	const unsigned size;
	const char *data;
} users_d[32] = {
	{
		true,
		"users.d/",
		"Asr3",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"Asr1",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"Asr2",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"admin",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"AsrCtrl",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"guest",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"User1",
		0,
		nullptr
	},
	{
		true,
		"users.d/",
		"Agent",
		0,
		nullptr
	},
	{
		false,
		"users.d/Asr1/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/Asr1/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/Asr1/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/admin/",
		"pairs.json",
		171,
		"{\n" \
		"    \"isAdmin\" : true,\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/admin/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/admin/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/guest/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/guest/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/guest/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/Agent/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/Agent/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/Agent/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/Asr3/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/Asr3/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/Asr3/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/AsrCtrl/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/AsrCtrl/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/AsrCtrl/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/Asr2/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/Asr2/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/Asr2/",
		"shared",
		0,
		nullptr
	},
	{
		false,
		"users.d/User1/",
		"pairs.json",
		149,
		"{\n" \
		"    \"token\" : \"b109f3bbbc244eb82441917ed06d618b9008dd09b3befd1b5e07394c706a8bb980b1d7785e5976ec049b46df5f1326af5a2ea6d103fd07c95385ffab0cacbc86\"\n" \
		"}\n"
	},
	{
		true,
		"users.d/User1/",
		"home",
		0,
		nullptr
	},
	{
		true,
		"users.d/User1/",
		"shared",
		0,
		nullptr
	}
};

#define USERS_D_INDEX	"{\"users.d/Agent\":7,\"users.d/Agent/home\":18,\"users.d/Agent/pairs.json\":17,\"users.d/Agent/shared\":19,\"users.d/Asr1\":1,\"users.d/Asr1/home\":9,\"users.d/Asr1/pairs.json\":8,\"users.d/Asr1/shared\":10,\"users.d/Asr2\":2,\"users.d/Asr2/home\":27,\"users.d/Asr2/pairs.json\":26,\"users.d/Asr2/shared\":28,\"users.d/Asr3\":0,\"users.d/Asr3/home\":21,\"users.d/Asr3/pairs.json\":20,\"users.d/Asr3/shared\":22,\"users.d/AsrCtrl\":4,\"users.d/AsrCtrl/home\":24,\"users.d/AsrCtrl/pairs.json\":23,\"users.d/AsrCtrl/shared\":25,\"users.d/User1\":6,\"users.d/User1/home\":30,\"users.d/User1/pairs.json\":29,\"users.d/User1/shared\":31,\"users.d/admin\":3,\"users.d/admin/home\":12,\"users.d/admin/pairs.json\":11,\"users.d/admin/shared\":13,\"users.d/guest\":5,\"users.d/guest/home\":15,\"users.d/guest/pairs.json\":14,\"users.d/guest/shared\":16}"

#endif // USERS_D_H
