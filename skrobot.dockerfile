#podman build -t SkRobot -f skrobot.dockerfile .
FROM debian:12

#podman build --platform linux/amd64 -t robot -f skrobot.dockerfile .
#FROM --platform=linux/arm64 debian:latest AS arm64_base
#FROM --platform=linux/amd64 debian:latest AS amd64_base
#FROM scratch
#COPY --from=arm64_base / /
#COPY --from=amd64_base / /

RUN apt-get update && apt-get upgrade -y

RUN apt-get install -y \
        git \
        build-essential \
        libogg-dev \
        libssl-dev \
        libvorbis-dev \
        libfftw3-dev \
        libespeak-dev \
        portaudio19-dev \
        libflac-dev \
        libflac++-dev \
        libvlc-dev \
        libzbar-dev \
        libopencv-dev \
        libi2c-dev \
        libstatgrab-dev \
        screen \
        redis 

RUN apt-get clean

RUN mkdir /opt/Sk && \
    mkdir /opt/Sk/src && \
    mkdir /opt/Sk/bin && \
    mkdir /opt/Sk/data

WORKDIR /opt/Sk/src

RUN git clone https://github.com/redis/hiredis.git && \
    git clone https://gitlab.com/Tetsuo-tek/SpecialK.git && \
    git clone https://gitlab.com/Tetsuo-tek/SkMake.git && \
    git clone https://gitlab.com/Tetsuo-tek/SkRobot.git

#RUN pwd && ls -l ./

WORKDIR /opt/Sk/src/hiredis
RUN make && \
    make install && \
    ldconfig

WORKDIR /opt/Sk/src/SkMake
RUN rm -f Makefile && \
    mv Makefile.dist Makefile && \
    make && \
    mv ./SkMake.bin /opt/Sk/bin/SkMake.bin && \
    rm -rf ./build
    
WORKDIR /opt/Sk/src/SkRobot
RUN /opt/Sk/bin/SkMake.bin -m skmake-shared-ubuntu.json -c 8 && \
    mv ./SkRobot-shared.bin /opt/Sk/bin/SkRobot.bin && \
    rm -rf ./build
    
RUN ln -s /opt/Sk/bin/SkMake.bin /usr/local/bin/skmake && \
    ln -s /opt/Sk/bin/SkRobot.bin /usr/local/bin/robot

#docker run -v /tmp/Robot:/tmp/Robot SkRobot
#docker run -v /path/on/host:/app/data SkRobot
#VOLUME /opt/Sk/data
#VOLUME /opt/Sk/src
#VOLUME /tmp/

EXPOSE 9000
EXPOSE 9001

CMD ["/usr/local/bin/robot"]

