# SkRobot

The SkRobot application, developed in C++ upon [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK) framework, offers a robust platform for managing data flows in autonomous devices, robotic systems and industrial production lines. It supports Input/Output management and custom internal network services, requiring a Unix-like environment (e.g., GNU/Linux or *BSD) and minimal dependencies, adhering to the POSIX standard. This setup ensures SkRobot is a flexible, easy-to-implement solution that can be adapted and modified even during production.


The main features of SkRobot can be briefly summarized in the following points:

- it is a hub for concentrating, connecting, and distributing flows and data from/to satellites;
- it creates two different types of channels that allow the distribution of data streams and on-demand micro-services, either synchronous or asynchronous;
- it creates the necessary pairs (explained below) along with the channels to distribute the application state (such as average pulse rates for modules and satellites or indices related to date, time, synchronization, events, logs, pair modification activities, etc.);
- the pairs are stored in various databases managed on the hub and created both internally within the core of the hub itself and externally by the satellites;
- each connected account is associated with an active database from the moment of connection until the closure of the last connection related to that account;
- each streaming and service channel is associated with a database owned by the account that created them (it remains active for the entire life of the channel);
- the databases support persistence in binary form on disk when requested and permitted;
- the satellites can run in different processes from SkRobot or within it as optional isolated threads, referred to as modules;
- the modules are more efficient in scheduling and therefore in mutual interaction because they are threads within SkRobot and are connected via a local socket but have a higher level of criticality (they can negatively impact SkRobot in its activities in case of anomalies or in situations where one module is processing too intensively compared to others and the involved CPU);
- support is provided for creating WUI content to be distributed with the same appearance across all active hotspots on different satellites and secondary hubs;
- through the http service, it is possible to create REST mountpoints that return Json content (or structured in other ways) and optionally accept url-queries for GET requests or forms for POST requests;
- the channels present in the FlowNetwork can be distributed by the web service in the form of http streaming;
- the creation, removal, and modification of databases and channels as well as interactions between satellites are subject to a permission system controlled on the main hub (only one hub in the entire DipoleNetwork has the accepted accounts and related permissions, which are distributed as an authentication service to the secondary hubs);
- if SkRobot is run with root permissions, it can authenticate satellites using the shadow authentication present in Linux systems (libshadow/libcrypt), distributing the underlying system authentication across the entire FlowNetwork.

You can learn how to make external-satellites by [examples](https://gitlab.com/Tetsuo-tek/SkRobot/-/tree/main/examples), using Sk/C++ and Sk/PySketch.

## Modular Architecture and Interface Classes

The architecture of SkRobot is modular. The functionalities offered by its core are those that enable and control communication with the FlowNetwork.

Modules, which are also satellites, can be added to the core but are internal to the hub with higher overall efficiency in communication and synchronization capabilities.
Modules follow the hub's activity from start to finish as their initialization and shutdown are directly controlled by the core.

They all derive from a common interface (SkAbstractModule) which facilitates and regulates interaction with the system. Based on this abstract interface, modules always provide a customizable Json configuration regarding the various internal parameters to be used.

During implementation and derivation, the interface also imposes the redefinition of some protected virtual methods that allow the module's state change and its control by the core, as well as the receipt of events from the flow network. Aside from the greater efficiency in communication and synchronization with the core and other modules, the functionality of the module is no different from that of an external satellite.

Relating to the development of external satellites, it is possible (though not mandatory) to include and derive the SkFlowSat class similar to SkAbstractModule. Deriving this class automatically includes all the connection and flow management procedures, distribution (publish and subscribe), and notifications for events from the flow network.

It also incorporates command-line management to launch the satellite from the terminal, to which other arguments can be added beyond those already included to change the address and connection type, user, password, log silencing, and more.
These two interface classes, to be derived to create modules and external satellites, lower the complexity of managing all aspects of events and networking, avoiding the need to implement it each time. This way, the code in the satellite program will always be dedicated solely to what is necessary.

The satellite can become very complex but only in relation to the functionalities that are to be implemented. The way the structuring of the deriving programs is enforced allows for comparable and orderly modules and satellites that behave uniformly concerning the network and service distribution, even under formal aspects.
To instantiate the required modules during configuration, SkRobot uses the introspection feature offered by Sk. Introspection also allows instantiation of Sk types by providing the new type name as a string, enabling the use of types not present at the time of writing the code.

SkRobot cannot directly include in its code the header files for the modules that can be developed ad-hoc as this information is not available during core development. Adding the necessary headers in SkRobot's code each time is not practical and they would be lost at the first update. Sk's introspection allows for the allocation of new types from the string name in SkRobot's Json configuration. The only thing SkRobot needs to know and include at compile time is the location of the module files in skmake.json when creating the Makefile. If SkRobot is recompiled, the new module (the class) will be known in the regenerated environment of the Sk application and will thus be instantiated by name.

The existing modules in the SpecialK framework today are: SkAudioCapture, SkCamera, SkVideoSource, SkMotionDetector, SkObjectDetector, SkCodeDetector.
By taking these modules as examples, the developer can create their own with any functionality not already covered.

## Flow protocol

SpecialK defines a communication protocol named the "flow protocol". It enables communication between entities (modules and satellites with their hubs) on various network supports, ranging from simple serial TTY lines to Unix-domain sockets, TCP, UDP, and WebSockets offered by the HTTP support.

In this context, communication uses a binary format, transporting structured frames for each command and response. The protocol distinguishes between synchronous and asynchronous commands, which are crucial for service distribution and flow management. Synchronous commands block until a response is received, while asynchronous commands do not wait and may send notification frames to all or only relevant connections based on the request type.

For efficiency, the binary frame lacks control records for data quality and integrity, relying instead on the correctness of parameters order and the announced segment length to ensure frame integrity. Any communication errors result in terminating the connection, logged as "Killing spurious client".

Due to the prevalence of little-endian (LE) hardware, all binary data within the frame is written in LE format, contrary to the big-endian (BE) Network-Order typically used in other protocols.

The `SkFlowServer` class manages these communications, supporting network functionalities and handling new connections, which can be either synchronous or asynchronous. Asynchronous connections facilitate the distribution of computational tasks across different processes and threads, aligning with the generic flow concept of the satellite.

On images reported below, the FlowNetwork graphs: Monocentric and Multicentric.

![Monocentric](doc/SkRobot.svg){width=45%}
![Multicentric](doc/FlowNetwork.svg){width=30%}


### Synchronous Portion of the Protocol: FLOW-SYNC

It is important to note that all connections received by the server always start as synchronous. Only after passing authentication is it possible to set the connection as asynchronous, transforming it into a satellite distribution stream.

Commands for the synchronous section, some of which are also valid for asynchronous connections, consist of those related to authentication and the management of the pair service offered through the `SkFlowPairDatabase` class:

- login execution;
- checking for existing database;
- setting the current database;
- creating a database;
- saving the database persistently;
- removing a database;
- requesting the entire content of the database;
- requesting a list of variables in the database;
- requesting the value of a variable;
- setting new values ​​on existing variables;
- creating new variables;
- removing variables;
- clearing the database;
- setting and requesting the value of optional properties for channels.

All operations directed to the database service are conditioned on setting the current database before being executed; setting the current database is done whenever the target for operations changes.

Variables are stored in the form of `SkVariant` type variables, a class capable of containing and expressing all primitive types, buffer pointers, and other container types such as strings, objects, maps, vectors, and lists. `SkVariant` can convert its contents to/from Json. Protocol commands using variables are duplicated with a less efficient counterpart that transports textual Json, as an adaptation to porting platforms where the `SkVariant` class like Python is not available yet.

Other synchronous commands directed at channels allow:

- obtaining the current list of channels;
- obtaining static properties of a specific channel;
- obtaining the header buffer for the streaming channel, if it exists;
- registering/deregistering for polling on a streaming channel;
- requesting the latest current buffer (polling) from a streaming channel;
- making synchronous requests to a service channel and receiving responses.

### Asynchronous Portion of the Protocol: FLOW-ASYNC

As mentioned earlier, asynchronous commands do not receive responses; at most, they can trigger the generation of asynchronous messages to one or many based on the requested command:

- setting the connection as asynchronous (originally it is always synchronous);
- creating a streaming or service channel;
- removing a channel of which one is the owner;
- requesting subscription to an active channel;
- requesting unsubscription from a still active channel (if the channel is removed for any reason, the unsubscription is automatic when removal is notified with an asynchronous message);
- attaching a replicating channel to a source channel (attach);
- detaching a replicating channel from a source channel (detach);
- requesting publication of data on created channels;
- setting the current database for the connection;
- creating a database;
- removing a database;
- saving the database persistently;
- setting new values ​​on existing variables;
- creating new variables;
- removing variables.

In operations related to databases and channels, getter commands (which expect a response) are missing compared to the synchronous portion. In these commands, requests for variable values ​​or checking if a database exists would typically be responses in the synchronous portion but are modified here to asynchronous notification messages.

When information or variables need to be obtained, a temporary synchronous connection can be created for blocking requests. The connection can be closed and the object destroyed as soon as the data is obtained.

In asynchronous connections, messages arrive from the server intercepting the following events (these are not the Sk events described earlier but events from the FlowNetwork):

- the current database has changed (only on the connection that set it);
- a channel has been created (broadcast to all);
- a channel has been removed (broadcast to all);
- a channel has modified its header (broadcast to all);
- a request has arrived on a previously created service channel (to the owner);
- a request to start publishing data on a specific channel of which one is the creator (to the channel owner);
- a request to stop publishing data on a specific channel of which one is the creator (to the channel owner);
- data has arrived from a subscribed channel (to all subscribers).

### Pairs databases

I refer to pair-databases simply as databases. A database is essentially a map of `SkVariant` variables indexed with string keys. Multiple databases can be activated in any instance of SkRobot; the data structure map is implemented with a Red-Black Binary Search Tree (BST) with logarithmic computational complexity.

Databases support file persistence when necessary. Stored data can survive application restarts if saved to file by the owner. These data containers allow storing and sharing real-time values from connections (streams and synchronous connections) or even from within SkRobot (core). Through this data, each connection can inform itself about the presence, consistency, and usefulness of other entities in the FlowNetwork as well as their state.

An audio and/or video consumer may want to know the parameters used for sampling incoming streams for audio (sampling frequency, sample type, channels, and PCM buffer length) and/or video (resolution and frames-per-second); they can simply request the relevant variables from databases associated with the respective channels to prepare for correct stream reception.

### Channel Distribution in FlowNetwork

Distribution occurs primarily through flow channels, which can be identified as data queues (1:N and 1:1) overlaid within an asynchronous connection described earlier as a satellite flow. Transmission over flow channels always occurs asynchronously to avoid interference in the passage of different types of data. In a FlowNetwork, up to 32768 flow channels can be activated, with `SkFlowChanID` being a redefinition of `int16_t`.

All channels in a FlowNetwork, regardless of type, are identified by three unique values (each taken individually):

- `chanID`
- `hashID`
- `name`

Channels can be of two types:

- **1:N** - intended for distributing streaming data of any kind that can be subscribed to and received by multiple consumers.
- **1:1** - intended to provide synchronous (or asynchronous) service of any type based on demand/response, with the possibility of creating one-to-one streaming as a result of service requests.

Disconnecting the flow on which the channel was created leads to the removal of the channel itself and all its established relationships in the system and distributed network with other satellites.

Only the owner and the administrator can remove a channel. When the core (the instance of `SkFlowServer` or the entity using it) is the creator of the channel, the channel's lifespan depends solely on intentional removal commands launched by an administrator, as well as the init and shutdown phases of the core itself.

#### Streaming Channels (SkFlowRedistrChannel) 1:N

Streaming channels are useful for distributing data produced by satellites and concentrators within the FlowNetwork so that they can be processed via synchronized processing by one or more receivers.

A channel is created and managed by what is referred to as the publisher role, which can be assumed by both satellites and concentrators. The publisher application streams the produced data onto the channel, which can be asynchronously utilized by one or more subscribers without requiring them through costly polling methods. Individual data buffers can still be requested through polling, albeit sporadically, to perform checks on ongoing production.

Distribution channels abstractly resume the concept of Signal/Slot interaction paradigm from [SpecialK](https://gitlab.com/Tetsuo-tek/SpecialK) and distribute it outside the application. PPublishing a new data buffer by the publisher is akin to triggering an operation similar to Signal emission transporting arguments (the publishing data). On the other hand, receiving the exported data on the subscribed queue by the subscriber is akin to invoking a connected Slot operation that carries data as arguments. Alternatively, one can parallelize the concept by considering that subscription (or its denial) by the subscriber equates to a local Attach operation (or vice versa Detach) between a Signal and a Slot.

This type of channel has additional properties required to identify the type of the transiting contents:

- Flow type (more general contents typing)
- Data type (among those supported by `SkVariant`)
- MIME type (default: application/octet-stream)
- Unit of measure for real physical values (default: ADIM)
- Minimum value (default: 0.f)
- Maximum value (default: 0.f)
- Production rate
- Possible redistribution point with HTTP protocol
- Any information related to encoding/decoding for multimedia streams
- Binary or textual header (default: empty)

Except for type-related properties, all others are optional. There are no checks on these values during production on the channel, so their meaning is purely declarative regarding the channel's identity. Data type, length, min/max, and MIME type related to the content being sent are not verified. The consumer trusts the correctness of the content sent by the producer. Nevertheless, consistency checks can be made on segment lengths, arrival time intervals for buffers, and data present in producer-owned databases. Developers may choose to underestimate the associated information. In systems with numerous channels and functionalities, inadequate informational oversight could create comprehension issues during debugging phases. Undervaluation of information can also generate ambiguity in data observation through any monitoring system capable of generating graphs or performing various types of automatic checks (in these cases, min/max values could be very useful).

Below is the graph for a real-time distributed processing use case of PCM audio, which clarifies the usage mode of this type of channels.

An example of audio distribution:

![Audio Example](doc/esempio1.svg){width=50% style="display: block; margin: 0 auto"}


For example, satellite A produces PCM float in streaming form, sending a fixed-length buffer containing captured audio every fraction of a second; another satellite B subscribes to the channel and receives incoming buffers, using them to encode them into OggVorbis format. A third consumer satellite C also subscribes to the same PCM buffers, performing a Fourier transform on the received signal. An additional satellite D subscribes to various channels of the same type but from different producers to create a mixer. The summed result for equivalent samples from all audio sources captured is then outputted in PCM buffer form.

Another graph related to real-time video distribution use case is as follows (shown in Figure 12): Satellite A captures frames from a camera and transmits them in Jpeg format at a predetermined rate (fps) over a channel. Satellite B subscribes to this channel, adept in detecting bounding boxes containing faces and converting these coordinates into serialized integers in an array to be outputted. B simultaneously sends the received frame, with detected boxes, on another channel. Additional satellite C subscribes to both the channel containing bounding box coordinates and the frame channel forwarded by the detector. C processes these frames to recognize identities, outputting a Json with potentially recognized identities along with their confidence levels on another channel created by C.

An example of video distribution:

![Video Example](doc/esempio2.svg){width=50% style="display: block; margin: 0 auto"}

Another proposed feature includes the possibility of replicating a source channel to other channels created by satellites that do not directly publish data. In this scenario, data is published on the source channel by its owner and replicated on other connected channels without the actual publication of their owner. It's crucial to note that only the channel creator can publish data on it. Replication linking and unlinking for streaming channels can be highly useful when a fixed-name source is required, and data produced by an unknown satellite (unable to write directly to the queue) needs to pass through it. The consumer satellite can create a channel and subscribe to it (self-subscription), assigning it the role of a replicating source. Any satellite with the rights can then link (attach) the true source to the replicating source intended for the consumer, allowing the latter to indirectly consume the data.

#### Service Channels (SkFlowServiceChannel) 1:1

Through this type of channel, it is possible to create custom micro-services on the flow network; any satellite can open a service channel and accept requests from other satellites. For example, a satellite could act as a file server and respond to requests for download, upload, removal, or information about files or folders, permissions modification, and so on.

The HTTP service distributed over the network can benefit from service channels; in fact, it is possible to distribute web content not only on the web port via HTTP protocol but also on the flow network. One or more dedicated satellites providing content can be used as specific and customized web servlets (microservices) for some type of activity or site effectively distributed over HTTP by another satellite coordinating requests and routing responses.

The service exported on this type of channel can be of any type, and the conversation with the requester always develops with a 1:1 ratio, even if the service remains available for multiple transactions simultaneously.

By its nature, the service channel can be grouped into clusters where multiple similar satellites with different channels offer the same service, and one of them handles concentrating requests and routing them to whoever among the others is freer for current computational density, including itself if others are heavily engaged.

The creator can make a request to their service channel, even though it makes little sense; however, the option has been preferred not to be removed.

A service request can be synchronous if made through the SkFlowSync client or asynchronous if made with SkFlowAsync; both types of clients allow the request by only changing the mode of consumption, with a direct and blocking response in the first case or with indirect and non-blocking notification in the second.

Below I show the graph related to [robot-nao-io](https://gitlab.com/Tetsuo-tek/robot-nao-io) project aimed at integrating [Nao-v5](https://www.aldebaran.com/it/nao) robot from Aldebaran. It requires a deprecated version of Python, but using SkRobot and [PySketch](https://gitlab.com/Tetsuo-tek/PySketch) the problem is solved, letting other collaborating satellites to perform data operation upon Python3 and other modern environments. The NaoSat still continue to work on python-2 using pynaoqi framework. Here it is important to see how other satellites can control Nao activities through the service channel open on it, diretected to receive commands.

An example of Nao robot integration, controlled by service channels and distributed by streaming channels:

![Nao Example](doc/esempioNao.svg){width=50% style="display: block; margin: 0 auto"}


## Install and configure

SkRobot can be simply installed through the [pck](https://gitlab.com/Tetsuo-tek/pck) manager:

```sh
pck --install SkRobot
```

If you would like to compile out of `pck`, you can follow standard [SkMake](https://gitlab.com/Tetsuo-tek/SkMake) procedure, selecting the right skmake.json file, or creating a new one.

Once SkRobot is installed, make the working directory:

```sh
mkdir Robot
cd Robot
```

Here it is possible launch SkRobot for the first time. By default SkRobot will start without any module, creating its filesystem inside this directory.

```sh
skrobot
```

SkRobot, on start, create the default robot.json configuration file, similar to the following:  

```json
{
    "appName" : "Robot",
    "checkTickTime" : 10,
    "enableTcpFs" : true,
    "fastTickTime" : 3000,
    "fsListenAddr" : "0.0.0.0",
    "fsListenPort" : 9000,
    "fsMaxConnQueued" : 10,
    "fsNodeCfgFile" : "",
    "httpDir" : "www",
    "httpIdleTimeoutInterval" : 20,
    "httpListenAddr" : "0.0.0.0",
    "httpListenPort" : 9001,
    "httpMaxConnQueued" : 10,
    "httpMaxHeaderSize" : 100000,
    "httpSslCertFile" : "ssl/selfsigned.crt",
    "httpSslEnabled" : false,
    "httpSslKeyFile" : "ssl/selfsigned.key",
    "modsCfgFile" : "",
    "slowTickTime" : 100000,
    "tickMode" : 2,
    "workingDir" : ""
}
```

Within this default configuration we can tune the robot activities also considering the underlaying hardware resources. Trhough this configuration SkRobot is seekable by local sockets on `/tmp/Robot` and by tcp sockets from any address on port 9000. Also HTTP service is enabled by default on port 9001, but without ssl support (you can enable it).
 
