#include "robotsensormeter.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    skApp->init(70000, 150000, SK_TIMEDLOOP_SLEEPING, false);

    RobotSensorMeter *a = new RobotSensorMeter;
    Attach(skApp->started_SIG, pulse, a, init, SkOneShotQueued);

    return skApp->exec();
}
