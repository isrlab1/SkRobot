#include "robotsensormeter.h"
#include <Core/System/skosenv.h>
#include <Core/Containers/skarraycast.h>
#include "gui.h"

ConstructorImpl(RobotSensorMeter, SkFlowAsync)
{
    window = nullptr;
    rx = nullptr;
    tx = nullptr;

    lcdDisplay = nullptr;
    gauge = nullptr;
    led = nullptr;

    setObjectName("RobotSensor");

    SlotSet(init);
    SlotSet(inputData);
    SlotSet(quit);

    SlotSet(fastTick);
    SlotSet(slowTick);
    SlotSet(oneSecTick);

    SlotSet(fullScreen);

    SlotSet(testToggleClick);
    SlotSet(testPulseClick);

    SlotSet(onDisconnection);

    Attach(skApp->fastZone_SIG, pulse, this, fastTick, SkQueued);
    Attach(skApp->slowZone_SIG, pulse, this, slowTick, SkQueued);
    Attach(skApp->oneSecZone_SIG, pulse, this, oneSecTick, SkQueued);

    Attach(this, channelDataAvailable, this, inputData, SkDirect);
    Attach(this, disconnected, this, onDisconnection, SkQueued);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSensorMeter, init)
{
    SilentSlotArgsWarning();

    SkStringList acceptedKeys;
    buildCLI(acceptedKeys);
    AssertKiller(!osEnv()->checkAllowedAppArguments(acceptedKeys));

    if (osEnv()->existsAppArgument("--help"))
    {
        cout << osEnv()->cmdLineHelp();
        exit(0);
    }

    buildUI();
    buildASync();
}

void RobotSensorMeter::closeApplication()
{
    window = nullptr;
    skApp->quit();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSensorMeter::buildASync()
{
    AssertKiller(!osEnv()->existsEnvironmentVar("ROBOT_ADDRESS"));
    SkString robotAddress = osEnv()->getEnvironmentVar("ROBOT_ADDRESS");

    if (robotAddress.startsWith("local:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("local:")];
        ObjectWarning("Selected Robot LOCAL-address [$ROBOT_ADDRESS]: " << robotAddress);

        AssertKiller(!localConnect(robotAddress.c_str()));
    }

    else if (robotAddress.startsWith("tcp:"))
    {
        robotAddress = &robotAddress.c_str()[strlen("tcp:")];
        ObjectWarning("Selected Robot TCP-address [$ROBOT_ADDRESS]: " << robotAddress);

        SkStringList robotAddressParsed;
        robotAddress.split(":", robotAddressParsed);
        AssertKiller(robotAddressParsed.count() < 1 || robotAddressParsed.count() > 2);

        if (robotAddressParsed.count() == 1)
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), 9000));

        else
            AssertKiller(!tcpConnect(robotAddressParsed.first().c_str(), robotAddressParsed.last().toInt()));
    }

    Attach(sck(),bytesWritten, tx, pulse, SkDirect);
    Attach(sck(),bytesRead, rx, pulse, SkDirect);

    SkString userName = "guest";
    SkString userPasswd = "password";

    /*cout << "\nRobot login:\nUsername: ";
    cin >> userName;

    cout << "Password: ";
    SkStdInput::unSetTermAttr(ECHO);
    cin >> userPasswd;
    SkStdInput::setTermAttr(ECHO);*/

    AssertKiller(!login(userName.c_str(), userPasswd.c_str()));
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSensorMeter::buildUI()
{
    window = DynCast(SkFltkDoubleWindow, ui->loadFromJSON(gui[0].data));
    Attach(window, closed, this, quit, SkQueued);

    lcdDisplay = DynCast(SkFltkLcdDisplay, ui->get("lcdDisplay"));
    gauge = DynCast(SkFltkGauge, ui->get("gauge"));
    gauge->setBounds(70.15, 70.4);
    led = DynCast(SkFltkLed, ui->get("led"));

    SkFltkLightButton *testToggleButton = DynCast(SkFltkLightButton, ui->get("testToggleButton"));
    Attach(testToggleButton, clicked, this, testToggleClick, SkDirect);

    SkFltkButton *testPulseButton = DynCast(SkFltkButton, ui->get("testPulseButton"));
    Attach(testPulseButton, clicked, this, testPulseClick, SkDirect);

    SkFltkLightButton *fsButton = DynCast(SkFltkLightButton, ui->get("fsButton"));
    Attach(fsButton, clicked, this, fullScreen, SkDirect);

    rx = DynCast(SkFltkLed, ui->get("rx"));
    tx = DynCast(SkFltkLed, ui->get("tx"));

    SkFltkButton *quitButton = DynCast(SkFltkButton, ui->get("quitButton"));
    Attach(quitButton, clicked, this, quit, SkDirect);

    window->show();
    //window->setSizeRange(window->size(), SkSize(0,0), 1, 1);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSensorMeter, fullScreen)
{
    SilentSlotArgsWarning();

    static bool fullscreen = false;

    if (!fullscreen)
        window->enableFullScreen();

    else
        window->disableFullScreen();

    window->redraw();
    fullscreen = !fullscreen;
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSensorMeter::onChannelAdded(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);
}

void RobotSensorMeter::onChannelRemoved(SkFlowChanID chanID)
{
    SkFlowChannel *ch = channel(chanID);
    AssertKiller(!ch);
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSensorMeter, inputData)
{
    SilentSlotArgsWarning();

    onFlowDataCome();
}

void RobotSensorMeter::onFlowDataCome()
{
    if (nextData())
    {
        SkFlowChannelData &d = getCurrentData();

        /*if (d.chanID == inputChan)
        {}*/
    }
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSensorMeter, testToggleClick)
{
    SilentSlotArgsWarning();
    led->toggle();
}

SlotImpl(RobotSensorMeter, testPulseClick)
{
    SilentSlotArgsWarning();
    led->pulse();
}

static float i=68.f;
//static int i=0;
SlotImpl(RobotSensorMeter, fastTick)
{
    SilentSlotArgsWarning();
}

SlotImpl(RobotSensorMeter, slowTick)
{
    SilentSlotArgsWarning();

    if (!lcdDisplay)
        return;

    i = skApp->getLastPulseElapsedTime()*1000;

    lcdDisplay->setValue(i);
    lcdDisplay->redraw();

    gauge->setValue(i);
    gauge->redraw();

    /*i += 0.1;
    //i++;

    if (i>72.f)
        i=68;*/
}

SlotImpl(RobotSensorMeter, oneSecTick)
{
    SilentSlotArgsWarning();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

SlotImpl(RobotSensorMeter, onDisconnection)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting on disconnection..");
    closeApplication();
}

SlotImpl(RobotSensorMeter, quit)
{
    SilentSlotArgsWarning();

    ObjectMessage("Quitting ..");
    closeApplication();
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

void RobotSensorMeter::buildCLI(SkStringList &acceptedKeys)
{
    acceptedKeys.append("--help");

    //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //

    osEnv()->addCliAppHelp("--help", "prints this output and exit");
}

//  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //  //
