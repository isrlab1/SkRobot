#ifndef ROBOTMONITOR_H
#define ROBOTMONITOR_H

#include <Core/App/skapp.h>
#include <Core/System/Network/FlowNetwork/skflowasync.h>

class RobotMonitor extends SkFlowAsync
{
    public:
        Constructor(RobotMonitor, SkFlowAsync);

        Slot(init);
        Slot(inputData);
        Slot(onDisconnection);
        Slot(exitFromKernelSignal);

    private:
        SkString pairsChanName;
        SkFlowChanID pairsChan;

        SkString eventsChanName;
        SkFlowChanID eventsChan;

        bool jsonOutput;

        void buildCLI(SkStringList &acceptedKeys);
        void buildASync();

        void onChannelAdded(SkFlowChanID chanID)                        override;
        void onChannelRemoved(SkFlowChanID chanID)                      override;
};

#endif // ROBOTMONITOR_H
