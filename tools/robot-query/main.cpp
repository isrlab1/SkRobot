#include "robotquery.h"

SkApp *skApp = nullptr;

int main(int argc, char *argv[])
{
    skApp = new SkApp(argc, argv);
    logger->enable(false);

    skApp->init(1, 250000, SK_FREELOOP, false);

    RobotQuery *a = new RobotQuery;
    Attach(skApp->started_SIG, pulse, a, init, SkOneShotDirect);

    return skApp->exec();
}
