#ifndef ROBOTQUERY_H
#define ROBOTQUERY_H

#include <Core/App/skapp.h>
#include <Core/System/Network/FlowNetwork/skflowsync.h>

class RobotQuery extends SkFlowSync
{
    public:
        Constructor(RobotQuery, SkFlowSync);

        Slot(init);

    private:
        void buildCLI(SkStringList &acceptedKeys);
        void buildSync();
        void execute();
};

#endif // ROBOTQUERY_H
